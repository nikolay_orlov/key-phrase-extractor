import requests
from thread import start_new_thread, allocate_lock


url = "http://192.168.99.100:8888/api/keywords?measure=1"
urlresults = "http://192.168.99.100:8888/api/performance"
testfiles = ['text1.txt', 'text2.txt', 'text3.txt']
threads = 1
repeats = 1
opened_threads = 0

def test():
    for r in range(0, repeats):
        for fn in testfiles:
            testWithFile(fn, threads)

def testWithFile(filename, threads):
    f = open(filename, "r")
    text = f.read()
    f.close()
    for i in range(0, threads):
        start_new_thread(request, (text,))             

def request(text):
    global opened_threads
    lock = allocate_lock()
    lock.acquire()    
    opened_threads+=1
    lock.release()

    r = requests.post(url, json={'text': text, 'keywords-limit': 20, 'relevance-threshold': 1})

    lock.acquire()    
    opened_threads-=1
    lock.release()    
    
print('start load test')
test()
while opened_threads > 0:
    pass

r = requests.get(urlresults)
f = open("performance.csv", "w")
f.write(r.text)
f.close()

print("done")