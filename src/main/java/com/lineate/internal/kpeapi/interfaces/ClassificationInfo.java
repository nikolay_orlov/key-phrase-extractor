package com.lineate.internal.kpeapi.interfaces;

/**
 * Classifier data
 */
public interface ClassificationInfo {
    String getClassifier();
    float getRank();
}
