package com.lineate.internal.kpeapi.interfaces;

import com.lineate.internal.kpeapi.exceptions.StrategyImplException;

import java.util.List;

/**
 * provide method for classification
 */
public interface ClassificationStrategy {

    void init(String serviceUrl);

    List<ClassificationInfo> classify(PipelineContext context, ComponentMetricsService metricsService) throws StrategyImplException;
}
