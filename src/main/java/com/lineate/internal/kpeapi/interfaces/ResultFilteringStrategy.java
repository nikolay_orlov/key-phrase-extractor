package com.lineate.internal.kpeapi.interfaces;

import com.lineate.internal.kpeapi.exceptions.StrategyImplException;

import java.util.List;

/**
 * result filtering: filter.keywords, filter.classifiers
 */
public interface ResultFilteringStrategy {
    List<KeywordInfo> filterKeywords(PipelineContext context) throws StrategyImplException;
    List<ClassificationInfo> filterClassifiers(PipelineContext context) throws StrategyImplException;
}

