package com.lineate.internal.kpeapi.interfaces;

import com.lineate.internal.kpeapi.metrics.HealthCodes;

/**
 * test run of pipeline and return diagnostic code
 */
public interface HealthCheckProvider {
    HealthCodes check();
}
