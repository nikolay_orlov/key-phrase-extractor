package com.lineate.internal.kpeapi.interfaces;

import com.lineate.internal.kpeapi.pipeline.ProcessingParameters;

import java.util.List;


/**
 * readonly interface of Pipeline context
 */
public interface PipelineContext {
    ProcessingParameters getParameters();
    String getInputData();
    String getText();
    List<KeywordInfo> getKeywords();
    List<ClassificationInfo> getClassifiers();
}
