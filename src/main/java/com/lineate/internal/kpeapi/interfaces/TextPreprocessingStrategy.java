package com.lineate.internal.kpeapi.interfaces;

import com.lineate.internal.kpeapi.exceptions.StrategyImplException;

/**
 * extract plain text from inputData
 */
public interface TextPreprocessingStrategy {
    String cleanup(PipelineContext context) throws StrategyImplException;
}
