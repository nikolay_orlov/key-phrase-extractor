package com.lineate.internal.kpeapi.interfaces;

import java.util.List;

/**
 * Keyword data
 */
public interface KeywordInfo {
    String getKeywordPhrase();
    int getWordsNumber();
    float getRank();
    List<String> getWords();
}
