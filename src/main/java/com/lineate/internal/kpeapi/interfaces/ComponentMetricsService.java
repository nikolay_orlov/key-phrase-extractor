package com.lineate.internal.kpeapi.interfaces;

/**
 * Service to save metrics for certain component
 * Implementation must knows which component provides this metric
 */
public interface ComponentMetricsService {
    void submitPerformanceMetric(double milliseconds);
}
