package com.lineate.internal.kpeapi.interfaces;

import com.lineate.internal.kpeapi.exceptions.StrategyImplException;

import java.util.List;

/**
 * extract keyword info from context.text
 */
public interface KeywordExtractorStrategy {

    void init(String serviceUrl);

    List<KeywordInfo> extractKeywords(PipelineContext context, ComponentMetricsService metricService) throws StrategyImplException;
}
