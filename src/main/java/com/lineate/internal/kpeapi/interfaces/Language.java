package com.lineate.internal.kpeapi.interfaces;

/**
 * supported languages
 */
public enum Language {
    unknown,
    en,
    fr,
    es,
    ru
}
