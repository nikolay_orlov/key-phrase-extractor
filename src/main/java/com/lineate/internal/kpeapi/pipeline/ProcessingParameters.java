package com.lineate.internal.kpeapi.pipeline;

import com.lineate.internal.kpeapi.interfaces.Language;

/**
 * Processing parameters
 */
public class ProcessingParameters {
    private int keywords_limit;
    private float rank_threshold;
    private Language language;

    /**
     * ctor
     * @param keywords_limit - max number of keywords in result
     * @param rank_threshold - minimal value of rank of keywords in results (value depends from algorithm)
     * @param language - language (supported language depends from algorithm)
     */
    public ProcessingParameters( int keywords_limit,
                                 float rank_threshold,
                                 Language language) {
        this.keywords_limit = keywords_limit;
        this.rank_threshold = rank_threshold;
        this.language = language;
    }

    public int getKeywords_limit() {
        return keywords_limit;
    }

    public float getRank_threshold() {
        return rank_threshold;
    }

    public Language getLanguage() {
        return language;
    }

}
