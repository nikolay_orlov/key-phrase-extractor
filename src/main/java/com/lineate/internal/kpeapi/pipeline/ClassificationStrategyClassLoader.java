package com.lineate.internal.kpeapi.pipeline;

import com.lineate.internal.kpeapi.interfaces.ClassificationStrategy;
import com.lineate.internal.kpeapi.interfaces.ComponentMetricsService;
import com.lineate.internal.kpeapi.model.ClassifierConfig;

/**
 * Created by ssakhno on 30.11.2017.
 */
public class ClassificationStrategyClassLoader {
    private static ClassificationStrategy impl = null;

    public static ClassificationStrategy load(ClassifierConfig cfg) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        if(impl == null || !cfg.getNlpAdapterClassName().toLowerCase().equals(impl.getClass().getName().toLowerCase())) {
            impl = (ClassificationStrategy) ClassificationStrategy.class
                    .getClassLoader()
                    .loadClass(cfg.getNlpAdapterClassName())
                    .newInstance();
            impl.init(cfg.getNlpServiceUrl());
        }
        return impl;
    }
}
