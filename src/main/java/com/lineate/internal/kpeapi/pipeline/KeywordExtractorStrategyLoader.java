package com.lineate.internal.kpeapi.pipeline;

import com.lineate.internal.kpeapi.interfaces.ComponentMetricsService;
import com.lineate.internal.kpeapi.interfaces.KeywordExtractorStrategy;
import com.lineate.internal.kpeapi.model.KeywordExtractorConfig;

/**
 * Created by ssakhno on 30.11.2017.
 */
public class KeywordExtractorStrategyLoader {
    private static KeywordExtractorStrategy impl = null;

    public static KeywordExtractorStrategy load(KeywordExtractorConfig cfg) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        if(impl == null || !cfg.getNlpAdapterClassName().toLowerCase().equals(impl.getClass().getName().toLowerCase())) {
            impl = (KeywordExtractorStrategy) KeywordExtractorStrategy.class
                    .getClassLoader()
                    .loadClass(cfg.getNlpAdapterClassName())
                    .newInstance();
            impl.init(cfg.getNlpServiceUrl());
        }
        return impl;
    }
}
