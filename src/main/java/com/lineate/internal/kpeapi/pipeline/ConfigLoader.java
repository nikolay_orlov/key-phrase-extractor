package com.lineate.internal.kpeapi.pipeline;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.lineate.internal.kpeapi.interfaces.Language;
import com.lineate.internal.kpeapi.model.PipelineConfig;
import com.lineate.internal.kpeapi.pipeline.ProcessingParameters;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Load config from file perodically.
 */
@Component
public class ConfigLoader  {
    private static Date lastLoadDate;
    private static long loadDelay = 10000; //10 seconds
    private static PipelineConfig cache = null;


    /**
     * load config from kpe-config.yml in working directory (if cache age > 10 seconds) or get from cache.
     * @return
     * @throws IOException
     */
    public PipelineConfig get() throws IOException {

        if(cache == null || getElapsedSeconds(lastLoadDate, new Date()) > loadDelay){
            File file = new File("kpe-config.yml");
            final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            lastLoadDate = new Date();
            cache = mapper.readValue(file, PipelineConfig.class);

        }
        return cache;
    }

    private long getElapsedSeconds(Date date1, Date date2) {
        if(date1 == null || date2==null) {
            return loadDelay + 1;
        }
        return date2.getTime() - date1.getTime();
    }

}
