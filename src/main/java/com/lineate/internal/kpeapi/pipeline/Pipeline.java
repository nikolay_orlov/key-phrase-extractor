package com.lineate.internal.kpeapi.pipeline;

import com.lineate.internal.kpeapi.exceptions.StrategyImplException;
import com.lineate.internal.kpeapi.interfaces.*;
import com.lineate.internal.kpeapi.metrics.AppMetricsService;
import com.lineate.internal.kpeapi.metrics.HealthCodes;
import com.lineate.internal.kpeapi.model.ProcessingResult;
import com.lineate.internal.kpeapi.strategies.NullClassifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.GaugeService;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Random;

/**
 * Processing com.lineate.internal.kpeapi.pipeline.
 */
public class Pipeline implements HealthCheckProvider {
    private TextPreprocessingStrategy preprocessing;
    private KeywordExtractorStrategy extractor;
    private ClassificationStrategy classifier;
    private ResultFilteringStrategy filter;
    private AppMetricsService appMetrics;

    private class StubComponentMetricService implements ComponentMetricsService{
        @Override
        public void submitPerformanceMetric(double milliseconds) {
        }
    }

    @Override
    public HealthCodes check() {
        Context ctx = new Context();
        ctx.setInputData("Health check");
        ProcessingParameters prms = new ProcessingParameters(1, 0, Language.unknown);
        ctx.setParameters(prms);

        try {
            ctx.setText(this.preprocessing.cleanup(ctx));
        } catch (Exception e){
            return HealthCodes.PreprocessorFails;
        }
        try{
        ctx.setKeywords(this.extractor.extractKeywords(ctx, new StubComponentMetricService()));
        } catch (Exception e){
            return HealthCodes.KeywordExtractionFails;
        }
        try{
            ctx.setClassifiers(this.classifier.classify(ctx, new StubComponentMetricService()));
        } catch (Exception e){
            return HealthCodes.ClassificationFails;
        }
        try{
            ctx.setKeywords(this.filter.filterKeywords(ctx));
            ctx.setClassifiers(this.filter.filterClassifiers(ctx));
        } catch (Exception e){
            return HealthCodes.FilteringFails;
        }
        return HealthCodes.OK;
    }

    private class Context implements PipelineContext {
        /**
         * settings of processing behavior
         */
        private ProcessingParameters parameters;
        /**
         * original input data:  web page content, url or something else that must be converted to plain text
         */
        private String inputData;
        /**
         * plain text data, ready for processing
         */
        private String text;
        /**
         * extracted keywords
         */
        private List<KeywordInfo> keywords;
        /**
         * resolved classifiers
         */
        private List<ClassificationInfo> classifiers;


        public ProcessingParameters getParameters() {
            return parameters;
        }

        public void setParameters(ProcessingParameters parameters) {
            this.parameters = parameters;
        }

        public String getInputData() {
            return inputData;
        }

        public void setInputData(String inputData) {
            this.inputData = inputData;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public List<KeywordInfo> getKeywords() {
            return keywords;
        }

        public void setKeywords(List<KeywordInfo> keywords) {
            this.keywords = keywords;
        }

        public List<ClassificationInfo> getClassifiers() {
            return classifiers;
        }

        public void setClassifiers(List<ClassificationInfo> classifiers) {
            this.classifiers = classifiers;
        }
    }


    /**
     * internal ctr
     * @param preprocessing - preprocessi0ng of input data, for example extract required plain text from Web page content, or from Wiki-markup.
     * @param extractor - extracts keywords
     * @param classifier - classifiers discovering
     * @param filter - filtering of results
     */
    Pipeline(
            TextPreprocessingStrategy preprocessing,
            KeywordExtractorStrategy extractor,
            ClassificationStrategy classifier,
            ResultFilteringStrategy filter,
            AppMetricsService appMetrics
    ){
        this.preprocessing = preprocessing;
        this.extractor = extractor;
        this.classifier = classifier;
        this.filter = filter;
        this.appMetrics = appMetrics;
    }

    /**
     * run com.lineate.internal.kpeapi.pipeline
     * @param text - text to extract keywords
     * @param prms - processing parameters
     * @return
     */
    public ProcessingResult run(String text,
                                ProcessingParameters prms,
                                boolean logPerformance
    ) throws StrategyImplException, IOException {
        Context ctx = new Context();
        ctx.setInputData(text);
        ctx.setParameters(prms);

        AppMetricsService.MetricsGroup grp = this.appMetrics.createMetricsGroup();

        grp.submitInputTextSizeMetric(text.length());

        ProcessingResult result = null;

        try(AppMetricsService.Measure m = grp.measureOverallProcessingPerformance()) {
            result = process(ctx, grp);
        }

        if(logPerformance){
            grp.saveLog();
        }

        return result;
    }

    private ProcessingResult process(Context ctx, AppMetricsService.MetricsGroup grp) throws StrategyImplException, IOException {
        try(AppMetricsService.Measure m = grp.measureTextPreprocessingPerformance()) {
            ctx.setText(this.preprocessing.cleanup(ctx));
        }
        try(AppMetricsService.Measure m = grp.measureKeywordExtractionPerformance()) {
            ctx.setKeywords(this.extractor.extractKeywords(ctx, grp.getPureKeywordExtractionMetricsService()));
        }
        try(AppMetricsService.Measure m = grp.measureClassificationPerformance()) {
            ctx.setClassifiers(this.classifier.classify(ctx, grp.getPureClassificationMetricsService()));
        }
        try(AppMetricsService.Measure m = grp.measureKeywordsFilteringPerformance()) {
            ctx.setKeywords(this.filter.filterKeywords(ctx));
        }
        try(AppMetricsService.Measure m = grp.measureClassifierFilteringPerformance()) {
            ctx.setClassifiers(this.filter.filterClassifiers(ctx));
        }

        ProcessingResult result = new ProcessingResult();
        result.setKeywords(ctx.getKeywords());
        result.setClassifiers(ctx.getClassifiers());
        return result;
    }

}
