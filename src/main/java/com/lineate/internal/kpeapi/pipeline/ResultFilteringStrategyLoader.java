package com.lineate.internal.kpeapi.pipeline;

import com.lineate.internal.kpeapi.interfaces.ResultFilteringStrategy;
import com.lineate.internal.kpeapi.model.ResultFilterConfig;

/**
 * Created by ssakhno on 30.11.2017.
 */
public class ResultFilteringStrategyLoader {
    private static ResultFilteringStrategy impl = null;

    public static ResultFilteringStrategy load(ResultFilterConfig cfg) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        if(impl == null || !cfg.getFilterClassName().toLowerCase().equals(impl.getClass().getName().toLowerCase())) {
            impl = (ResultFilteringStrategy) ResultFilteringStrategy.class
                    .getClassLoader()
                    .loadClass(cfg.getFilterClassName())
                    .newInstance();
        }
        return impl;
    }
}
