package com.lineate.internal.kpeapi.pipeline;

import com.lineate.internal.kpeapi.interfaces.TextPreprocessingStrategy;
import com.lineate.internal.kpeapi.model.TextPreprocessorConfig;

/**
 * Created by ssakhno on 30.11.2017.
 */
public class TextPreprocessingStrategyLoader {
    private static TextPreprocessingStrategy impl = null;

    public static TextPreprocessingStrategy load(TextPreprocessorConfig cfg) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        if(impl == null || !cfg.getPreprocessorClassName().toLowerCase().equals(impl.getClass().getName().toLowerCase())) {
            impl = (TextPreprocessingStrategy) TextPreprocessingStrategy.class
                    .getClassLoader()
                    .loadClass(cfg.getPreprocessorClassName())
                    .newInstance();
        }
        return impl;
    }
}
