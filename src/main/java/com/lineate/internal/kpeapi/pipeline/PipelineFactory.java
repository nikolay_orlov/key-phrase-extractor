package com.lineate.internal.kpeapi.pipeline;

import com.lineate.internal.kpeapi.metrics.AppMetricsService;
import com.lineate.internal.kpeapi.model.PipelineConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.lineate.internal.kpeapi.strategies.NullClassifier;

import java.io.IOException;

/**
 * create com.lineate.internal.kpeapi.pipeline with com.lineate.internal.kpeapi.strategies implementations.
 */
@Component
public class PipelineFactory {
    @Autowired
    private ConfigLoader configLoader;
    @Autowired
    private AppMetricsService appMetrics;

    /**
     * create com.lineate.internal.kpeapi.pipeline
     * @return
     * @throws IOException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    public Pipeline create() throws IOException, IllegalAccessException, InstantiationException, ClassNotFoundException {

        PipelineConfig cfg = configLoader.get();
        Pipeline pipeline = new Pipeline(
                TextPreprocessingStrategyLoader.load(cfg.getPreprocessorConfig()),
                KeywordExtractorStrategyLoader.load(cfg.getKeywordsExtractorConfig()),
                ClassificationStrategyClassLoader.load(cfg.getClassifierConfig()),
                ResultFilteringStrategyLoader.load(cfg.getFilterConfig()),
                appMetrics
        );
        return pipeline;
    }

    public Pipeline createKeyphraseExtractionOnly() throws IOException, IllegalAccessException, InstantiationException, ClassNotFoundException {

        PipelineConfig cfg = configLoader.get();
        Pipeline pipeline = new Pipeline(
                TextPreprocessingStrategyLoader.load(cfg.getPreprocessorConfig()),
                KeywordExtractorStrategyLoader.load(cfg.getKeywordsExtractorConfig()),
                new NullClassifier(),
                ResultFilteringStrategyLoader.load(cfg.getFilterConfig()),
                appMetrics
        );
        return pipeline;
    }


}
