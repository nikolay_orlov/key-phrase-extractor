package com.lineate.internal.kpeapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Application calls
 */
@SpringBootApplication(scanBasePackages = "com.lineate.internal.kpeapi")
@EnableScheduling
//@Import({ApplicationConfiguration.class, SwaggerConfiguration.class, MapperConfiguration.class, RepositoryConfiguration.class})
public class KeyphraseExtractorApplication extends SpringBootServletInitializer {

    private static final Logger LOGGER = LoggerFactory.getLogger(KeyphraseExtractorApplication.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(KeyphraseExtractorApplication.class);
    }

    public static void main(String[] args) {
        try {
            SpringApplication.run(KeyphraseExtractorApplication.class, args);
        }
        catch (Throwable t){
            LOGGER.error("Failed to start");
            throw t;
        }
    }
}

