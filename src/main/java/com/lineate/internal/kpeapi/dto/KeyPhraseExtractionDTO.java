package com.lineate.internal.kpeapi.dto;

/**
 * DTO
 */
public class KeyPhraseExtractionDTO {

    private KeywordDTO[] keywords;

    public KeywordDTO[] getKeywords() {
        return keywords;
    }

    public void setKeywords(KeywordDTO[] keywords) {
        this.keywords = keywords;
    }
}
