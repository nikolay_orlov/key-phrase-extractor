package com.lineate.internal.kpeapi.dto;

/**
 * Keyword info
 */
public class KeywordDTO {

    private String text;
    private float relevance;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public float getRelevance() {
        return relevance;
    }

    public void setRelevance(float relevance) {
        this.relevance = relevance;
    }
}
