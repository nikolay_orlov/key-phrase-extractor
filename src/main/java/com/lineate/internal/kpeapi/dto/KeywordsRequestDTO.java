package com.lineate.internal.kpeapi.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by ssakhno on 01.12.2017.
 */
public class KeywordsRequestDTO {
    @JsonProperty("text")
    private String text;

    @JsonProperty("keywords-limit")
    private int keywordsLimit;

    @JsonProperty("relevance-threshold")
    private float relevanceThreshold;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getKeywordsLimit() {
        return keywordsLimit;
    }

    public void setKeywordsLimit(int keywordsLimit) {
        this.keywordsLimit = keywordsLimit;
    }

    public float getRelevanceThreshold() {
        return relevanceThreshold;
    }

    public void setRelevanceThreshold(float relevanceThreshold) {
        this.relevanceThreshold = relevanceThreshold;
    }
}
