package com.lineate.internal.kpeapi.endpoints;

import com.lineate.internal.kpeapi.KeyphraseExtractorApplication;
import com.lineate.internal.kpeapi.dto.KeyPhraseExtractionDTO;
import com.lineate.internal.kpeapi.dto.KeywordDTO;
import com.lineate.internal.kpeapi.dto.KeywordsRequestDTO;
import com.lineate.internal.kpeapi.exceptions.StrategyImplException;
import com.lineate.internal.kpeapi.metrics.CsvFileMetricsWriter;
import com.lineate.internal.kpeapi.model.ProcessingResult;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import com.lineate.internal.kpeapi.services.DtoMapper;
import com.lineate.internal.kpeapi.services.KeyphraseExtraction;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.stream.Collectors;

/**
 * keyphrase extraction endpoint
 */
@RestController
@RequestMapping("/api")
public class KeyphraseExtractionEndpoint {
    private static final Logger LOGGER = LoggerFactory.getLogger(KeyphraseExtractorApplication.class);

    @Autowired
    private KeyphraseExtraction service;

    @Autowired
    private DtoMapper mapper;

    @PostMapping(value = "/keywords", produces = MediaType.APPLICATION_JSON_VALUE)
    public KeyPhraseExtractionDTO extractKeyPhrases(
            final @RequestParam(name = "measure", defaultValue = "0") byte measure,
            final @RequestBody KeywordsRequestDTO prms
    ) throws ClassNotFoundException, IOException, InstantiationException, IllegalAccessException, StrategyImplException {
        ProcessingResult data = service.extract(
                prms.getText(),
                prms.getKeywordsLimit(),
                prms.getRelevanceThreshold(),
                measure != 0);
        KeyPhraseExtractionDTO dto = new KeyPhraseExtractionDTO();
        dto.setKeywords(data.getKeywords().stream()
                .map(x -> mapper.map(x))
                .collect(Collectors.toList())
                .toArray(new KeywordDTO[0]));
        return dto;
    }


}
