package com.lineate.internal.kpeapi.endpoints;

import com.lineate.internal.kpeapi.KeyphraseExtractorApplication;
import com.lineate.internal.kpeapi.metrics.CsvFileMetricsWriter;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * special endpoint for retrieving measure report
 */
@RestController
@RequestMapping("/api")
public class MeasureEndpoint {
    private static final Logger LOGGER = LoggerFactory.getLogger(KeyphraseExtractorApplication.class);

    @GetMapping(value = "/performance")
    public void getPerformanceLog(HttpServletResponse response) throws IOException {
        InputStream is = null;
        try {
            response.setContentType("text/csv");
            //XXX blocking read! Not critical for this purposes, but leads to errors during save data in performance.csv
            is = new FileInputStream(CsvFileMetricsWriter.fileName);
            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
            CsvFileMetricsWriter.reset(); //start writing from blank
        } catch (FileNotFoundException e) {
            response.sendError(404, "performance.csv is not found");
        } finally{
            if (is != null) {
                is.close();
            }
        }
    }
}
