package com.lineate.internal.kpeapi.strategies;

import com.lineate.internal.kpeapi.interfaces.ClassificationInfo;
import com.lineate.internal.kpeapi.interfaces.ClassificationStrategy;
import com.lineate.internal.kpeapi.interfaces.ComponentMetricsService;
import com.lineate.internal.kpeapi.interfaces.PipelineContext;

import java.util.LinkedList;
import java.util.List;

/**
 * Stub classifier
 */
public class NullClassifier implements ClassificationStrategy {

    @Override
    public void init(String serviceUrl) {
        //ignore
    }

    @Override
    public List<ClassificationInfo> classify(PipelineContext context, ComponentMetricsService metricService ) {
        return new LinkedList<ClassificationInfo>(); //return empty list
    }
}
