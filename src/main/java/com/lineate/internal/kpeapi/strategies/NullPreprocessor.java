package com.lineate.internal.kpeapi.strategies;

import com.lineate.internal.kpeapi.interfaces.PipelineContext;
import com.lineate.internal.kpeapi.interfaces.TextPreprocessingStrategy;

/**
 * Null-implementation of text preprocessing strategy
 */
public class NullPreprocessor implements TextPreprocessingStrategy{

    @Override
    public String cleanup(PipelineContext context) {
        return context.getInputData();
    }
}
