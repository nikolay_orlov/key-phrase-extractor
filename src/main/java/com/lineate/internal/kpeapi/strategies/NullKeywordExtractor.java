package com.lineate.internal.kpeapi.strategies;

import com.lineate.internal.kpeapi.interfaces.KeywordExtractorStrategy;
import com.lineate.internal.kpeapi.interfaces.KeywordInfo;
import com.lineate.internal.kpeapi.interfaces.ComponentMetricsService;
import com.lineate.internal.kpeapi.interfaces.PipelineContext;

import java.util.LinkedList;
import java.util.List;

/**
 * Stub for keyword extractor
 */
public class NullKeywordExtractor implements KeywordExtractorStrategy {
    @Override
    public void init(String serviceUrl) {
        //ignore
    }

    @Override
    public List<KeywordInfo> extractKeywords(PipelineContext context, ComponentMetricsService metricService) {
        return new LinkedList<KeywordInfo>();
    }
}
