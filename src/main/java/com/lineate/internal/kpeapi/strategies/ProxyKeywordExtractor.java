package com.lineate.internal.kpeapi.strategies;

import com.lineate.internal.kpeapi.exceptions.StrategyImplException;
import com.lineate.internal.kpeapi.interfaces.*;
import com.mashape.unirest.http.*;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.body.RequestBodyEntity;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Proxy for keyword extractor, redirect query to NLP service and expect data in correct format
 */
public class ProxyKeywordExtractor implements KeywordExtractorStrategy {
    private String url;

    @Override
    public void init(String serviceUrl) {
        this.url = serviceUrl;
    }

    @Override
    public List<KeywordInfo> extractKeywords(
            PipelineContext context, ComponentMetricsService metricService
    ) throws StrategyImplException {
        JSONObject bodyObj = new JSONObject();
        bodyObj.put("text", context.getText());
        bodyObj.put("keywords-limit", context.getParameters().getKeywords_limit());
        bodyObj.put("relevance-threshold", context.getParameters().getRank_threshold());

        RequestBodyEntity request = Unirest.post(this.url).header("accept", "application/json").body(bodyObj);

        HttpResponse<String> response;
        try {
            response = request.asString();
        } catch (UnirestException e) {
            throw new StrategyImplException("Fail proxy keyword extraction to '" + url + "': " + e.getMessage(), e);
        }

        if (response.getStatus() / 100 != 2) {
            throw new StrategyImplException(String.format(
                    "Service '%s' returns %d, %s", url, response.getStatus(), response.getBody()
            ));
        }

        JsonNode body;
        try {
            body = new JsonNode(StringUtils.trim(response.getBody()));
        } catch (RuntimeException e) {
            throw new StrategyImplException(String.format(
                    "Can't parse JSON received from '%s': '%s'.", url, response.getBody()
            ), e);
        }

        JSONArray keywords = body.getObject().getJSONArray("keywords");
        //save performance metric
        if (body.getObject().has("duration")) {
            Double duration = body.getObject().getDouble("duration");
            metricService.submitPerformanceMetric(duration);
        }

        List<KeywordInfo> result = new LinkedList<>();
        for (int keywordIndex = 0; keywordIndex < keywords.length(); ++keywordIndex) {
            KeywordInfoImpl kwi = new KeywordInfoImpl();
            JSONObject kw = keywords.getJSONObject(keywordIndex);
            kwi.setKeywordPhrase(kw.getString("text"));
            kwi.setRank((float) kw.getDouble("relevance"));
            result.add(kwi);
        }
        return result;
    }

    private static class KeywordInfoImpl implements KeywordInfo {
        private static final Pattern WORDS_SPLIT_PATTERN = Pattern.compile("\\s*[a-zA-Z0-9]+\\s*");

        private String keywordPhrase;
        private float rank;

        @Override
        public String getKeywordPhrase() {
            return keywordPhrase;
        }

        @Override
        public int getWordsNumber() {
            return WORDS_SPLIT_PATTERN.split(keywordPhrase).length;
        }

        public void setKeywordPhrase(String keywordPhrase) {
            this.keywordPhrase = keywordPhrase;
        }

        @Override
        public float getRank() {
            return rank;
        }

        @Override
        public List<String> getWords() {
            return Arrays.asList(WORDS_SPLIT_PATTERN.split(keywordPhrase));
        }

        public void setRank(float rank) {
            this.rank = rank;
        }
    }
}
