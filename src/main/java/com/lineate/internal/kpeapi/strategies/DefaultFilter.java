package com.lineate.internal.kpeapi.strategies;

import com.lineate.internal.kpeapi.interfaces.ClassificationInfo;
import com.lineate.internal.kpeapi.interfaces.KeywordInfo;
import com.lineate.internal.kpeapi.interfaces.PipelineContext;
import com.lineate.internal.kpeapi.interfaces.ResultFilteringStrategy;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Implements default filter that cut results with Processing parameters
 */
public class DefaultFilter implements ResultFilteringStrategy {
    @Override
    public List<KeywordInfo> filterKeywords(PipelineContext context) {
        return context.getKeywords().stream()
                .filter(k -> k.getRank() >= context.getParameters().getRank_threshold())
                .sorted((k1, k2)->Float.compare(k2.getRank(), k1.getRank())) //reverse
                .limit(context.getParameters().getKeywords_limit())
                .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<ClassificationInfo> filterClassifiers(PipelineContext context) {
        return context.getClassifiers().stream()
                .sorted((k1, k2)->Float.compare(k2.getRank(), k1.getRank())) //reverse
                .limit(context.getParameters().getKeywords_limit())
                .collect(Collectors.toCollection(LinkedList::new));
    }
}
