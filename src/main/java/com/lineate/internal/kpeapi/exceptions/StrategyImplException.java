package com.lineate.internal.kpeapi.exceptions;

/**
 * Created by ssakhno on 30.11.2017.
 */
public class StrategyImplException extends Exception {

    public StrategyImplException(String message){
        super(message);
    }

    public StrategyImplException(String message, Exception inner){
        super(message, inner);
    }

}
