package com.lineate.internal.kpeapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *  result filter config
 */
public class ResultFilterConfig {

    @JsonProperty("class-name")
    private String filterClassName;

    public String getFilterClassName() {
        return filterClassName;
    }

    public void setFilterClassName(String filterClassName) {
        this.filterClassName = filterClassName;
    }
}
