package com.lineate.internal.kpeapi.model;

import com.lineate.internal.kpeapi.interfaces.ClassificationInfo;
import com.lineate.internal.kpeapi.interfaces.KeywordInfo;

import java.util.List;

/**
 * Created by ssakhno on 30.11.2017.
 */
public class ProcessingResult {
    /**
     * resolved keywords
     */
    private List<KeywordInfo> keywords;

    /**
     * resolved classifiers
     */
    private List<ClassificationInfo> classifiers;

    public List<KeywordInfo> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<KeywordInfo> keywords) {
        this.keywords = keywords;
    }

    public List<ClassificationInfo> getClassifiers() {
        return classifiers;
    }

    public void setClassifiers(List<ClassificationInfo> classifiers) {
        this.classifiers = classifiers;
    }
}
