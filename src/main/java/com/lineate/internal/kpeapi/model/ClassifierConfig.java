package com.lineate.internal.kpeapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Classifier adapter config
 */
public class ClassifierConfig {

    @JsonProperty("class-name")
    private String nlpAdapterClassName;

    @JsonProperty("service-url")
    private String nlpServiceUrl;


    public String getNlpAdapterClassName() {
        return nlpAdapterClassName;
    }

    public void setNlpAdapterClassName(String nlpAdapterClassName) {
        this.nlpAdapterClassName = nlpAdapterClassName;
    }

    public String getNlpServiceUrl() {
        return nlpServiceUrl;
    }

    public void setNlpServiceUrl(String nlpServiceUrl) {
        this.nlpServiceUrl = nlpServiceUrl;
    }
}
