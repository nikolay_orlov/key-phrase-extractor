package com.lineate.internal.kpeapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * config for input pre-processing to cleanup and extract text
 */
public class TextPreprocessorConfig {

    @JsonProperty("class-name")
    private String preprocessorClassName;

    public String getPreprocessorClassName() {
        return preprocessorClassName;
    }

    public void setPreprocessorClassName(String preprocessorClassName) {
        this.preprocessorClassName = preprocessorClassName;
    }
}
