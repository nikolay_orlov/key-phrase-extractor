package com.lineate.internal.kpeapi.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * config structure
 */
public class PipelineConfig {
        @JsonProperty("preprocessor")
        private TextPreprocessorConfig preprocessorConfig;

        @JsonProperty("keywords-extractor")
        private KeywordExtractorConfig keywordsExtractorConfig;

        @JsonProperty("classifier")
        private ClassifierConfig classifierConfig;

        @JsonProperty("filter")
        private ResultFilterConfig filterConfig;

        public TextPreprocessorConfig getPreprocessorConfig() {
                return preprocessorConfig;
        }

        public void setPreprocessorConfig(TextPreprocessorConfig preprocessorConfig) {
                this.preprocessorConfig = preprocessorConfig;
        }

        public KeywordExtractorConfig getKeywordsExtractorConfig() {
                return keywordsExtractorConfig;
        }

        public void setKeywordsExtractorConfig(KeywordExtractorConfig keywordsExtractorConfig) {
                this.keywordsExtractorConfig = keywordsExtractorConfig;
        }

        public ClassifierConfig getClassifierConfig() {
                return classifierConfig;
        }

        public void setClassifierConfig(ClassifierConfig classifierConfig) {
                this.classifierConfig = classifierConfig;
        }

        public ResultFilterConfig getFilterConfig() {
                return filterConfig;
        }

        public void setFilterConfig(ResultFilterConfig filterConfig) {
                this.filterConfig = filterConfig;
        }
}
