package com.lineate.internal.kpeapi.services;

import com.lineate.internal.kpeapi.dto.KeywordDTO;
import com.lineate.internal.kpeapi.interfaces.KeywordInfo;
import org.springframework.stereotype.Service;

/**
 * Mapper class
 */
@Service
public class DtoMapper {

    /**
     * map KeywordInfo to KeywordDTO
     * @param kw
     * @return
     */
    public KeywordDTO map(KeywordInfo kw) {
        KeywordDTO dto = new KeywordDTO();
        dto.setText(kw.getKeywordPhrase());
        dto.setRelevance(kw.getRank());
        return dto;
    }
}
