package com.lineate.internal.kpeapi.services;

import com.lineate.internal.kpeapi.exceptions.StrategyImplException;
import com.lineate.internal.kpeapi.interfaces.Language;
import com.lineate.internal.kpeapi.model.ProcessingResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.lineate.internal.kpeapi.pipeline.Pipeline;
import com.lineate.internal.kpeapi.pipeline.PipelineFactory;
import com.lineate.internal.kpeapi.pipeline.ProcessingParameters;

import java.io.IOException;

/**
 * Service to run com.lineate.internal.kpeapi.pipeline
 */
@Service
public class KeyphraseExtraction {
    private Language language = Language.unknown;

    @Autowired
    PipelineFactory factory;

    /**
     * Extract keyphrase from input
     * @param input
     * @param keywords_limit
     * @param rank_threshold
     * @return
     */
    public ProcessingResult extract(String input, int keywords_limit, float rank_threshold, boolean logPerformance) throws ClassNotFoundException, InstantiationException, IllegalAccessException, IOException, StrategyImplException {
        Pipeline pipeline = factory.createKeyphraseExtractionOnly();
        ProcessingParameters prms = new ProcessingParameters(
            keywords_limit,
            rank_threshold,
            language
        );
        return pipeline.run(input, prms, logPerformance);
    }
}
