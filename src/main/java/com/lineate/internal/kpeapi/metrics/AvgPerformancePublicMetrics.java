package com.lineate.internal.kpeapi.metrics;

import com.codahale.metrics.*;
import com.codahale.metrics.Timer;
import com.sun.javafx.collections.MappingChange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.PublicMetrics;
import org.springframework.boot.actuate.metrics.Metric;
import org.springframework.boot.actuate.metrics.reader.MetricRegistryMetricReader;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.StreamSupport;

/**
 * Avg calculation of metrics
 */
public class AvgPerformancePublicMetrics implements PublicMetrics {

    @Autowired
    private MetricRegistry registry;

    @Override
    public Collection<Metric<?>> metrics() {

        List<Metric<?>> result = new LinkedList<>();

        result.add(createAvgMetric(AppMetricsService.TextSizeHistMetricName));
        result.add(createAvgMetric(AppMetricsService.MeasureTextPreprocessingTimerMetricName));
        result.add(createAvgMetric(AppMetricsService.MeasureKeywordExtractionTimerMetricName));
        result.add(createAvgMetric(AppMetricsService.MeasurePureKeywordExtractionTimerMetricName));
        result.add(createAvgMetric(AppMetricsService.MeasureClassificationTimerMetricName));
        result.add(createAvgMetric(AppMetricsService.MeasurePureClassificationTimerMetricName));
        result.add(createAvgMetric(AppMetricsService.MeasureKeywordsFilteringTimerMetricName));
        result.add(createAvgMetric(AppMetricsService.MeasureClassifiersFilteringTimerMetricName));
        result.add(createAvgMetric(AppMetricsService.MeasureOverallProcessingTimerMetricName));

        return null;
    }



    private Metric<?> createAvgMetric(String name){
        SortedMap<String, Timer> timers = registry.getTimers(AppMetricsService.getFilter());
        Timer timer =  timers.get(timers.firstKey());
        Metric<?> m = new Metric<Number>(
                name + ".avg",
                timer.getSnapshot().getMean()
                );
        return m;
    }

}
