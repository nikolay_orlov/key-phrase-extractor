package com.lineate.internal.kpeapi.metrics;

/**
 * Health codes
 */
public enum HealthCodes {
    OK,
    PipelineConfigurationFails,
    PreprocessorFails,
    KeywordExtractionFails,
    ClassificationFails,
    FilteringFails
}
