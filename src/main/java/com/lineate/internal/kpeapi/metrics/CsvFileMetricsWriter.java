package com.lineate.internal.kpeapi.metrics;

import com.codahale.metrics.*;
import com.codahale.metrics.Timer;
import com.lineate.internal.kpeapi.KeyphraseExtractorApplication;
import com.opencsv.CSVWriter;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.Metric;
import org.springframework.boot.actuate.metrics.reader.MetricRegistryMetricReader;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Write metrics to performance.csv
 * It should be used only in measuring mode (api/keywords/?measure=1). So not use it in regular pipeline processing since it use lock()/unlock()
 */
@Component
public class CsvFileMetricsWriter {

    public static String fileName = "performance.csv";
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(KeyphraseExtractorApplication.class);
    private static volatile boolean isFirstRun = true;
    public static void reset(){
        isFirstRun = true;
    }

    String[] headers = {"timestamp", "text size", "preprocessing", "keywords extraction", "pure keywords extraction", "classification", "pure classification", "keywords filtering", "classifiers filtering", "overall"};
    private boolean append = true;

    private ReentrantLock lock = new ReentrantLock();

    public CsvFileMetricsWriter() {
    }

    public void saveMetrics(Date timestamp, Map<String, Double> metrics) {
        try {
            lock.lock();
            try(FileWriter writer = new FileWriter(fileName,  !isFirstRun)){
                try(CSVWriter csvWriter = new CSVWriter(writer)) {
                    if(isFirstRun) {
                        csvWriter.writeNext(headers);
                        isFirstRun = false;
                    }
                    String timeStampRepr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S").format(timestamp);
                    String[] rec = {
                            timeStampRepr,
                            getMetric(metrics, AppMetricsService.TextSizeHistMetricName),
                            getMetric(metrics, AppMetricsService.MeasureTextPreprocessingTimerMetricName),
                            getMetric(metrics, AppMetricsService.MeasureKeywordExtractionTimerMetricName),
                            getMetric(metrics, AppMetricsService.MeasurePureKeywordExtractionTimerMetricName),
                            getMetric(metrics, AppMetricsService.MeasureClassificationTimerMetricName),
                            getMetric(metrics, AppMetricsService.MeasurePureClassificationTimerMetricName),
                            getMetric(metrics, AppMetricsService.MeasureKeywordsFilteringTimerMetricName),
                            getMetric(metrics, AppMetricsService.MeasureClassifiersFilteringTimerMetricName),
                            getMetric(metrics, AppMetricsService.MeasureOverallProcessingTimerMetricName)
                    };
                    csvWriter.writeNext(rec);
                }
            }
        } catch (IOException e) {
            LOGGER.error("Fails while save metrics in performance.csv", e);
        } finally{
            lock.unlock();
        }
    }

    private String getMetric(Map<String, Double> metrics, String name){
        if(metrics.containsKey(name)){
            return String.valueOf(metrics.get(name));
        }
        return "";
    }
}
