package com.lineate.internal.kpeapi.metrics;

import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.lineate.internal.kpeapi.interfaces.ComponentMetricsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.Metric;
import org.springframework.stereotype.Service;

import java.io.Closeable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Helper service to write metrics
 */
@Service
public class AppMetricsService {

    public static String MeasureTextPreprocessingTimerMetricName = "kpe-api.text-preprocessing.performance";
    public static String MeasureKeywordExtractionTimerMetricName = "kpe-api.keyword-extraction.performance";
    public static String MeasureClassificationTimerMetricName = "kpe-api.classification.performance";
    public static String MeasureKeywordsFilteringTimerMetricName = "kpe-api.keywords-filtering.performance";
    public static String MeasureClassifiersFilteringTimerMetricName = "kpe-api.classifiers-filtering.performance";
    public static String MeasurePureKeywordExtractionTimerMetricName = "kpe-api.pure-keyword-extraction.performance";
    public static String MeasurePureClassificationTimerMetricName = "kpe-api.pure-classification.performance";
    public static String TextSizeHistMetricName = "kpe-api.text-size";
    public static String MeasureOverallProcessingTimerMetricName = "kpe-api.processing.performance";

    public static String[] metricNames = {
            TextSizeHistMetricName,
            MeasureTextPreprocessingTimerMetricName,
            MeasureKeywordExtractionTimerMetricName,
            MeasurePureKeywordExtractionTimerMetricName,
            MeasureClassificationTimerMetricName,
            MeasurePureClassificationTimerMetricName,
            MeasureKeywordsFilteringTimerMetricName,
            MeasureClassifiersFilteringTimerMetricName,
            MeasureOverallProcessingTimerMetricName};

    private static class AppMetricFilter implements MetricFilter {

        @Override
        public boolean matches(String s, com.codahale.metrics.Metric metric) {
            return Arrays.asList(AppMetricsService.metricNames).stream().filter(x->x.equals(s)).findFirst().isPresent();
        }
    }

    public static MetricFilter getFilter(){
        return new AppMetricFilter();
    }

    /**
     * Basic class for implementations that used in try-with-resource block around measurable code block
     */
    public abstract class Measure implements Closeable {

        private MetricRegistry registry;
        private Timer timer;
        private Timer.Context context;
        private MetricsGroup grp;
        private String name;
        private Date startDate;

        public Measure(MetricsGroup grp, MetricRegistry registry, String name){
            this.name = name;
            this.grp = grp;
            timer = registry.timer(name);
            context = timer.time();
            startDate = new Date();
        }

        @Override
        public void close() throws IOException {
            context.stop();
            grp.saveMetric(name, new Date().getTime()-startDate.getTime());
        }
    }

    private MetricRegistry registry;
    private CsvFileMetricsWriter metricsWriter;

    @Autowired
    AppMetricsService(MetricRegistry registry, CsvFileMetricsWriter metricsWriter){
        this.registry = registry;
        this.metricsWriter = metricsWriter;
    }

    class MeasureTextPreprocessing extends Measure {

        public MeasureTextPreprocessing(MetricsGroup grp, MetricRegistry registry) {
            super(grp, registry, MeasureTextPreprocessingTimerMetricName);
        }
    }

    class MeasureKeywordExtraction extends Measure {
        public MeasureKeywordExtraction(MetricsGroup grp, MetricRegistry registry) {
            super(grp, registry, MeasureKeywordExtractionTimerMetricName);
        }
    }

    class MeasureClassification extends Measure {
        public MeasureClassification(MetricsGroup grp, MetricRegistry registry) {
            super(grp, registry, MeasureClassificationTimerMetricName);
        }
    }

    class MeasureKeywordsFiltering extends Measure {
        public MeasureKeywordsFiltering(MetricsGroup grp, MetricRegistry registry) {
            super(grp, registry, MeasureKeywordsFilteringTimerMetricName);
        }
    }

    class MeasureClassifiersFiltering extends Measure {
        public MeasureClassifiersFiltering(MetricsGroup grp, MetricRegistry registry) {
            super(grp, registry, MeasureClassifiersFilteringTimerMetricName);
        }
    }

    class MeasureOverallProcessing extends Measure {
        public MeasureOverallProcessing(MetricsGroup grp, MetricRegistry registry) {
            super(grp, registry, MeasureOverallProcessingTimerMetricName);
        }
    }

    class MeasurePureKeywordExtraction implements ComponentMetricsService {
        private MetricRegistry registry;
        private MetricsGroup grp;

        public MeasurePureKeywordExtraction(MetricsGroup grp, MetricRegistry registry){
            this.registry = registry;
            this.grp = grp;
        }

        @Override
        public void submitPerformanceMetric(double milliseconds) {
            Timer timer = registry.timer(MeasurePureKeywordExtractionTimerMetricName);
            timer.update((long)milliseconds, TimeUnit.MILLISECONDS);
            grp.saveMetric(AppMetricsService.MeasurePureKeywordExtractionTimerMetricName, milliseconds);
        }
    }

    class MeasurePureClassification implements ComponentMetricsService {
        private MetricRegistry registry;
        private MetricsGroup grp;

        public MeasurePureClassification(MetricsGroup grp, MetricRegistry registry){
            this.registry = registry;
            this.grp = grp;
        }

        @Override
        public void submitPerformanceMetric(double milliseconds) {
            Timer timer = registry.timer(MeasurePureClassificationTimerMetricName);
            timer.update((long)milliseconds, TimeUnit.MILLISECONDS);
            grp.saveMetric(AppMetricsService.MeasurePureClassificationTimerMetricName, milliseconds);
        }
    }

    public class MetricsGroup {
        private Date timestamp = new Date();
        private HashMap<String, Double> metrics = new HashMap<String, Double>();
        private AppMetricsService service;
        private long uid;

        MetricsGroup(AppMetricsService service, long uid){
            this.uid = uid;
            this.service = service;
        }

        public void submitInputTextSizeMetric(int bytesCount){
            Histogram hist = registry.histogram(TextSizeHistMetricName);
            hist.update(bytesCount);
            metrics.put(TextSizeHistMetricName, (double) bytesCount);
        }

        public ComponentMetricsService getPureKeywordExtractionMetricsService(){
            return new MeasurePureKeywordExtraction(this, service.registry);
        }

        public ComponentMetricsService getPureClassificationMetricsService(){
            return new MeasurePureClassification(this, service.registry);
        }

        public Measure measureTextPreprocessingPerformance(){
            return new MeasureTextPreprocessing(this, service.registry);
        }

        public Measure measureKeywordsFilteringPerformance(){
            return new MeasureKeywordsFiltering(this, service.registry);
        }

        public Measure measureClassifierFilteringPerformance(){
            return new MeasureClassifiersFiltering(this, service.registry);
        }

        public Measure measureKeywordExtractionPerformance(){
            return new MeasureKeywordExtraction(this, service.registry);
        }

        public Measure measureClassificationPerformance(){
            return new MeasureClassification(this, service.registry);
        }

        public Measure measureOverallProcessingPerformance(){
            return new MeasureOverallProcessing(this, service.registry);
        }

        /**
         * save performance log
         */
        public void saveLog(){
            metricsWriter.saveMetrics(this.timestamp, this.metrics);
        }

        void saveMetric(String name, double value){
            metrics.put(name, value);
        }


    }

    public MetricsGroup createMetricsGroup(){
        long uid = Thread.currentThread().getId();
        return new MetricsGroup(this, uid);
    }


}
