package com.lineate.internal.kpeapi.metrics;

import com.lineate.internal.kpeapi.pipeline.Pipeline;
import com.lineate.internal.kpeapi.pipeline.PipelineFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Health indicator
 */
@Component
public class AppHealth implements HealthIndicator {

    @Autowired
    PipelineFactory factory;

    @Override
    public Health health() {
        HealthCodes errorCode = check(); // perform some specific health check
        if (errorCode != HealthCodes.OK) {
            return Health.down().withDetail("Check error:", errorCode).build();
        }
        return Health.up().build();
    }

    private HealthCodes check() {
        Pipeline pipeline = null;
        try {
            pipeline = factory.createKeyphraseExtractionOnly();
        } catch (Exception e) {
            return HealthCodes.PipelineConfigurationFails;
        }

        return pipeline.check();
    }
}