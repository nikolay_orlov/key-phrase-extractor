from flask import Flask
from flask import request
from json import encoder
from topia.termextract import tag
from topia.termextract import extract
import time

__author__ = 's_sakhno'

app = Flask(__name__)
tagger = tag.Tagger()
tagger.initialize()


def extractKeyphrases(data):
    extractor = extract.TermExtractor(tagger)
    extractor.filterConfig = extract.permissiveFilter
    result = extractor(data)
    return result


def filterKeyphrases(keyphrases, rank_threshold, keywords_limit):
    filtered_data = list(filter(lambda x: x[1] >= rank_threshold, keyphrases))
    ordered_data = sorted(filtered_data, key=lambda k: rank(k[1], k[2]))
    cut_data = ordered_data[-keywords_limit:]
    return map(lambda itm: (itm[0], rank(itm[1], itm[2])), cut_data)


def rank(frequency, words_number):
    return frequency + 5 * words_number


@app.route("/keywords", methods=['POST'])
def keywords():
    data = request.get_json(force=True)
    text = data["text"]
    if "relevance-threshold" in data:
        rank_threshold = data["relevance-threshold"]
    else:
        rank_threshold = 0.001
    keywords_limit = data["keywords-limit"]
    starttime = time.time()
    phrases = extractKeyphrases(text)
    stoptime = time.time()
    keywords = filterKeyphrases(phrases, rank_threshold, keywords_limit)

    keywords_json = u",".join(map(
        lambda x: u"{{\"text\": {0}, \"relevance\": {1}}}".format(encoder.encode_basestring(x[0]), x[1]), keywords
    ))

    json = u"{{\"keywords\": [{0}], \"duration\": {1}}}".format(keywords_json, str((stoptime - starttime) * 1000))
    return json


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8888)
