from flask import Flask
from flask import request
from json import encoder
import time
import rake

__author__ = 's_sakhno'

stoppath = "SmartStoplist.txt"

app = Flask(__name__)


@app.route("/health", methods=['GET'])
def health():
    return "", 200


@app.route("/keywords", methods=['POST'])
def keywords():
    data = request.get_json(force=True)
    text = data["text"]
    if "relevance-threshold" in data:
        rank_threshold = data["relevance-threshold"]
    else:
        rank_threshold = 0.001
    keywords_limit = data["keywords-limit"]
    starttime = time.time()
    rake_object = rake.Rake(stoppath, 5, 2, 1)
    stoptime = time.time()
    keywords = list(filter(lambda y: y[1] >= rank_threshold, rake_object.run(text)))[0:keywords_limit - 1]

    keywords_json = u",".join(map(
        lambda x: u"{{\"text\": {0}, \"relevance\": {1}}}".format(encoder.encode_basestring(x[0]), x[1]), keywords
    ))

    json = u"{{\"keywords\": [{0}], \"duration\": {1}}}".format(keywords_json, str((stoptime - starttime) * 1000))
    return json


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8888)
