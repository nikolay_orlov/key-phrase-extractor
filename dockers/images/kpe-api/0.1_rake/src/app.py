from flask import Flask
from flask import request
import requests
import time
import os


__author__ = 's_sakhno'

app = Flask(__name__)

def readConfig():
    with open('service-config.cfg', 'r') as myfile:
        for line in myfile:
            return line          
    return ''

@app.route("/health", methods=['GET'])
def health():
    return "OK",200

@app.route("/keywords", methods=['POST'])
def keywords():
    data = request.get_json(force=True)
    text = data["text"]    
    rank_threshold = data["rank_threshold"]    
    keywords_limit = data["keywords_limit"]
    #nlpservice = os.environ['NLP_ADDRESS']
    nlpservice = readConfig()
    r = requests.post(nlpservice, json={'text': text, 'rank_threshold': rank_threshold, 'keywords_limit':  keywords_limit})    
    if r.status_code != 200:
        return "Error to call '" + nlpservice + "' : " + r.reason, r.status_code
    else:
        return r.text

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)

