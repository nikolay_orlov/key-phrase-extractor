KPE_VERSION=0.3

pushd ../../../..
mvn clean package
if [ ! -f target/kpe-api-${KPE_VERSION}-SNAPSHOT.jar ]; then
    read -n 1 -p "Can't build KPE API. Press any key to continue..."
    exit 1
fi
popd

mkdir lib
cp ../../../../target/kpe-api-${KPE_VERSION}-SNAPSHOT.jar lib

docker stop kpe-api
docker rm kpe-api
docker rmi kpe_api_0.3_rake
docker build --no-cache -t kpe_api_0.3_rake .
