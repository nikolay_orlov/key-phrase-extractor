KeyPhrase Extraction app


Documentation:
Index:         https://docs.google.com/document/d/1sQTlqU6eOvXfaO_mQx9IQj7RTLZhO7RBmzrJgiFAwJg/
Project tasks: https://docs.google.com/document/d/1uZzvisUYS-vhkZO9I2JLb_D-bgDZXGBpzJ6Ewjkc3_0
NLP guide:     https://docs.google.com/document/d/1z2WB-4n70uFlwx-bmUvlVKYtleL0LgYhdmOwpfFCwA0
Dockers guide: https://docs.google.com/document/d/1IY-76lmefuICBd7oy8TJbJDOkUN5UFRE_kpdhC6Cel0/edit#heading=h.osktfrreiodg


Pre-requirements:
JDK 8
Maven
Docker v1.13+
CURL (for quick testing) you can use Postman or any similar tool


1. Build Java app:
mvn[ clean] package


2. Copy built jar into appropriate folder to build docker image:
copy ./target/kpe-api-0.3-SNAPSHOT.jar ./dockers/images/kpe-api/0.3_rake/lib/kpe-api-0.3-SNAPSHOT.jar


3. Build docker kpe-api image (use Docker CLI).
Please make sure that dockers/images/kpe-api/0.3_rake/cfg/kpe-config.yml has actual configuration data

cd dockers/images/kpe-api/0.3_rake
docker build -t kpe_api_0.3_rake .
cd ../../../


4. Build docker rake image (use Docker CLI):
cd dockers/images/rake
docker build -t rake_1.0_en .
cd ../../../


5. Compose services and run containers (use Docker CLI)
cd dockers/services/kpe_app_0.3
docker-compose up -d


6. Smoke test services (just examples of usage, the actual working requests may differ)
#test rake
curl -H "Content-Type: application/json" -X GET http://192.168.99.100:4040/health
curl -H "Content-Type: application/json" -X POST -d "{'text' : 'This is a test string for test the service','relevance-threshold': 1,'keywords-limit':20}" http://192.168.99.100:4040/keywords

#test kpe-api
curl -H "Content-Type: application/json" -X POST -d "{'text' : 'This is a test string for test the service','relevance-threshold': 1,'keywords-limit':20}" http://192.168.99.100:8888/api/keywords
